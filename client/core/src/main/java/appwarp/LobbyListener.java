package appwarp;

import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.LobbyRequestListener;

/**
 * Created by dan on 7/3/19.
 */

public class LobbyListener implements LobbyRequestListener {
    private WarpController callBack;

    public LobbyListener(WarpController callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onJoinLobbyDone(LobbyEvent lobbyEvent) {
        callBack.onJoinLobbyDone(lobbyEvent);
    }

    @Override
    public void onLeaveLobbyDone(LobbyEvent lobbyEvent) {
        callBack.onLeaveLobbyDone(lobbyEvent);
    }

    @Override
    public void onSubscribeLobbyDone(LobbyEvent lobbyEvent) {

    }

    @Override
    public void onUnSubscribeLobbyDone(LobbyEvent lobbyEvent) {

    }

    @Override
    public void onGetLiveLobbyInfoDone(LiveRoomInfoEvent liveRoomInfoEvent) {
        callBack.onGetLiveLobbyInfoDone(liveRoomInfoEvent);
    }
}
