package appwarp;


import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;


public class WarpController {
    public static String APP_KEY = "6df784ac-c84e-4f50-9";
    public static String HOST_NAME =  "127.0.0.1";


    private final ConnectionListener connectionListener;
    private final ZoneListener zoneListener;
    private RoomListener roomListener;
    private final LobbyListener lobbyListener;
    private WarpListener warpListener;
    public WarpClient warpClient;

	public String roomId = "";
    public String[] roomIds;
    public byte joinResultCode = -1;
    public byte lobbyResultCode = -1;
    private String[] lobbyUsers;


    public WarpController() {
        // add the warp listeners
        connectionListener = new ConnectionListener(this);
        zoneListener = new ZoneListener(this);
        roomListener = new RoomListener(this);
        lobbyListener = new LobbyListener(this);


        // init the warp client
        WarpClient.initialize(APP_KEY, HOST_NAME);
        try {
            warpClient = WarpClient.getInstance();
            warpClient.addConnectionRequestListener(connectionListener);
            warpClient.addZoneRequestListener(zoneListener);
            warpClient.addRoomRequestListener(roomListener);
            warpClient.addLobbyRequestListener(lobbyListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onConnectDone(boolean status, String s){
        warpListener.onConnectDone(status);
    }

    public void onDisconnectDone(boolean status){
    }

    void onGetAllRoomsDone(AllRoomsEvent result) {
        if(result != null) {
            if (result.getRoomIds() != null){
                roomIds = result.getRoomIds();
                warpListener.onGetAllRoomsDone(roomIds);
            } else {
            }
        }
    }

    public void onGetLiveUserInfoDone(String customData, String playerId, String roomId) {
        warpListener.onLiveUserInfo(customData, playerId, roomId);
    }

    protected void onGetLiveLobbyInfoDone(LiveRoomInfoEvent event) {
        lobbyUsers = event.getJoinedUsers();
        warpListener.onGetLiveLobbyInfoDone(lobbyUsers);
    }

    public void onLeaveRoomDone(RoomEvent event) {
        roomId = "";
        warpListener.onLeaveRoom();
    }

    void onJoinRoomDone(RoomEvent event) {
        joinResultCode = event.getResult();
        switch (joinResultCode) {
            case WarpResponseResultCode.SUCCESS:
                roomId = event.getData().getId();
                warpListener.onJoinRoom(roomId);
                break;
            default:
                break;
        }
    }

    public void setWarpListener(WarpListener warpListener) {
        this.warpListener = warpListener;
    }

    public void onJoinLobbyDone(LobbyEvent lobbyEvent) {
        lobbyResultCode = lobbyEvent.getResult();
    }

    public void onLeaveLobbyDone(LobbyEvent lobbyEvent) {
        lobbyResultCode = lobbyEvent.getResult();
    }
}
