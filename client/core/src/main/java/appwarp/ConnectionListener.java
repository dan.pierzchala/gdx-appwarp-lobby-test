package appwarp;

import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener;

import static com.toodangood.gdxappwarpclient.Utils.log;

public class ConnectionListener implements ConnectionRequestListener {
	private final String TAG = "ConnectionListener";
	WarpController callBack;
	
	public ConnectionListener(WarpController callBack){
		this.callBack = callBack;
	}

	@Override
	public void onConnectDone(ConnectEvent e, String s) {
		log(TAG, "onConnectDone: " + s);
		if(e.getResult()==WarpResponseResultCode.SUCCESS){
			callBack.onConnectDone(true, "");
		}else{
			log(TAG, "onConnectDone ERROR:" + e.getResult() + " - " + s);
			callBack.onConnectDone(false, s);
		}
	}

	public void onDisconnectDone(ConnectEvent e) {
		if(e.getResult() == WarpResponseResultCode.SUCCESS) {
			callBack.onDisconnectDone(true);
		} else {
			callBack.onDisconnectDone(false);
		}
	}

	@Override
	public void onInitUDPDone (byte result) {
	}

}
