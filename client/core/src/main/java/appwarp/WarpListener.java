package appwarp;


public interface WarpListener {
	void onConnectDone(boolean status);
	void onLiveUserInfo(String customData, String playerId, String locationId);
	void onGetLiveLobbyInfoDone(String[] users);
	void onGetAllRoomsDone(String[] roomIds);
	void onJoinRoom(String roomId);
	void onLeaveRoom();
}
