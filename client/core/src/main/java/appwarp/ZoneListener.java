package appwarp;

import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener;



public class ZoneListener implements ZoneRequestListener{
	private static final String TAG = "ZoneListener";
	private WarpController callBack;

	public ZoneListener(WarpController callBack) {
		this.callBack = callBack;
	}

	@Override
	public void onDeleteRoomDone(RoomEvent roomEvent, String s) {
	}

	@Override
	public void onCreateRoomDone(RoomEvent roomEvent, String s) {
	}

	@Override
	public void onRPCDone(byte b, String s, Object o) {
	}


	@Override
	public void onGetAllRoomsDone (AllRoomsEvent event) {
		if (event.getResult()==WarpResponseResultCode.SUCCESS){
			callBack.onGetAllRoomsDone(event);
		} else {
			callBack.onGetAllRoomsDone(null);
		}
	}

	@Override
	public void onGetLiveUserInfoDone (LiveUserInfoEvent event) {
		callBack.onGetLiveUserInfoDone(event.getCustomData(), event.getName(), event.getLocationId());
	}

	@Override
	public void onGetMatchedRoomsDone (MatchedRoomsEvent me) {

	}

	@Override
	public void onGetOnlineUsersDone (AllUsersEvent event) {
	}

	@Override
	public void onSetCustomUserDataDone (LiveUserInfoEvent arg0) {
	}

}
