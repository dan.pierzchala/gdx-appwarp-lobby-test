package com.toodangood.gdxappwarpclient;

import com.badlogic.gdx.Gdx;

public class Utils {
    public static void log(String tag, String text) {
        Gdx.app.log(tag, text);
    }
}
