package com.toodangood.gdxappwarpclient;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;

import appwarp.WarpController;
import appwarp.WarpListener;

import static com.toodangood.gdxappwarpclient.Utils.log;

/** {@link com.badlogic.gdx.ApplicationListener} implementation shared by all platforms. */
public class GdxAppwarpClient extends ApplicationAdapter implements WarpListener {
	private static final String USERNAME = "testuser";
	private String TAG = "GdxAppwarpClient";
	private Stage stage;
	private Skin skin;
	private WarpController warpController;
	private Label connectionStatusLabel;
	private Label roomsLabel;
	private TextButton joinRoom;
	private Label roomLabel;
	private Label roomResponseCode;
	private TextButton leaveRoom;
	private Label lobbyResponseCode;
	private Label lobbyUsers;
	private Label myLocationLabel;

	@Override
	public void create() {
		// create and initialize the WarpController
		warpController = new WarpController();
		warpController.setWarpListener(this);

		int spacing = 10;


		stage = new Stage(new FitViewport(640, 480));
		skin = new Skin(Gdx.files.internal("ui/skin.json"));

		Table table = new Table();
		table.setFillParent(true);

		stage.addActor(table);

		Label label = new Label("Example Appwarp Demo", skin);
		table.add(label);
		table.row();

		String serverInfo = String.format("host: %s      zone: %s", warpController.HOST_NAME, warpController.APP_KEY);
		Label serverInfoLabel = new Label(serverInfo, skin);
		table.add(serverInfoLabel);
		table.row();

		connectionStatusLabel = new Label("Connection Status: 0", skin);
		table.add(connectionStatusLabel);
		table.row();

		TextButton button = new TextButton("Connect", skin);
		button.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onConnectClicked();
			}
		});
		table.add(button);
		table.row();


		TextButton button3 = new TextButton("Disconnect", skin);
		button3.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onDisconnectClicked();
			}
		});
		table.add(button3).spaceBottom(spacing);
		table.row();


		myLocationLabel = new Label("My location: ", skin);
		table.add(myLocationLabel);
		table.row();

		TextButton getUsers = new TextButton("Get my location", skin);
		getUsers.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				warpController.warpClient.getLiveUserInfo(USERNAME);
			}
		});
		table.add(getUsers);
		table.row();

		Label usersLabel = new Label("", skin);
		table.add(usersLabel);
		table.row();


		roomsLabel = new Label("Room Ids: ", skin);
		table.add(roomsLabel);
		table.row();

		roomLabel = new Label("Room Status: Not in room", skin);
		table.add(roomLabel);
		table.row();
		roomResponseCode = new Label("Room Response Code: ", skin);
		table.add(roomResponseCode);
		table.row();

		TextButton button2 = new TextButton("Get Rooms", skin);
		button2.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onGetRoomsClicked();
			}
		});
		table.add(button2);
		table.row();


		joinRoom = new TextButton("Join Room", skin);
		joinRoom.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onJoinRoomClicked();
			}
		});
		table.add(joinRoom);
		table.row();

		leaveRoom = new TextButton("Leave Current Room", skin);
		leaveRoom.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onLeaveCurrentRoomClicked();
			}
		});
		table.add(leaveRoom).spaceBottom(spacing);
		table.row();


		Label lobbyHeader = new Label("Lobby commands", skin);
		table.add(lobbyHeader);
		table.row();

		lobbyResponseCode = new Label("Lobby response code", skin);
		table.add(lobbyResponseCode);
		table.row();

		lobbyUsers = new Label("Lobby users: ", skin);
		table.add(lobbyUsers);
		table.row();

		TextButton getLobbyInfo = new TextButton("Get Lobby Info", skin);
		getLobbyInfo.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onGetLobbyInfo();
			}
		});
		table.add(getLobbyInfo);
		table.row();

		TextButton joinLobby = new TextButton("Join Lobby", skin);
		joinLobby.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onJoinLobby();
			}
		});
		table.add(joinLobby);
		table.row();

		TextButton leaveLobby = new TextButton("Leave Lobby", skin);
		leaveLobby.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				onLeaveLobby();
			}
		});
		table.add(leaveLobby);
		table.row();



		Gdx.input.setInputProcessor(stage);
	}


	@Override
	public void render() {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		connectionStatusLabel.setText(getConnectionStatusString(warpController.warpClient.getConnectionState()));
		roomResponseCode.setText("Room response code: " + warpController.joinResultCode);
		lobbyResponseCode.setText("Lobby response code: " + warpController.lobbyResultCode);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	/*
	http://appwarps2.shephertz.com/dev-center/api-guide/java-api-reference/#getconnectionstate
	 */
	private String getConnectionStatusString(int connectionState) {
		String status = "";
		switch (connectionState) {

			case 0:
				status = "Connected";
				break;
			case 1:
				status = "Connecting";
				break;
			case 2:
				status = "Disconnected";
				break;
		}
		return String.format("Connection Status: %s(%s)", connectionState, status);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

	private void onConnectClicked() {
		warpController.warpClient.connectWithUserName(USERNAME, "authdata");
	}

	private void onDisconnectClicked() {
		warpController.warpClient.disconnect();
	}

	private void onGetRoomsClicked() {
		warpController.warpClient.getAllRooms();
	}

	private void onJoinRoomClicked() {
		try {
			warpController.warpClient.joinRoom(warpController.roomIds[0]);
		} catch (NullPointerException e) {

		}
	}

	private void onLeaveCurrentRoomClicked() {
		warpController.warpClient.leaveRoom(warpController.roomId);
	}

	private void onJoinLobby() {
		warpController.warpClient.joinLobby();
	}

	private void onLeaveLobby() {
		warpController.warpClient.leaveLobby();
	}

	private void onGetLobbyInfo() {
		warpController.warpClient.getLiveLobbyInfo();
	}


	@Override
	public void onConnectDone(boolean status) {

	}

	@Override
	public void onLiveUserInfo(String customData, String playerId, final String locationId) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				String locString = "My Location: ";
				if (locationId != null) {
					locString += locationId;
				}
				myLocationLabel.setText(locString);
			}
		});
	}


	@Override
	public void onGetAllRoomsDone(final String[] roomIds) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				if (roomIds.length > 0) {
					joinRoom.setText("Join room: " + roomIds[0]);
				}
			}
		});
		roomsLabel.setText("Room ids: " + ", ".join(", ", roomIds));
	}

	@Override
	public void onJoinRoom(final String roomId) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				roomLabel.setText("Room Status: Joined room " + roomId);
			}
		});
	}

	@Override
	public void onLeaveRoom() {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				roomLabel.setText("Room Status: Not in room");
			}
		});
	}

	@Override
	public void onGetLiveLobbyInfoDone(final String[] users) {
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				int count = (users != null) ? users.length : 0;
				lobbyUsers.setText("Lobby User Count: " + count);
			}
		});
	}
}