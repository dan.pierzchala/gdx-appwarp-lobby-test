package com.toodangood.gdxappwarpserver.adaptors;

import com.shephertz.app42.server.idomain.BaseZoneAdaptor;
import com.shephertz.app42.server.idomain.HandlingResult;
import com.shephertz.app42.server.idomain.IRoom;
import com.shephertz.app42.server.idomain.IUser;
import com.shephertz.app42.server.idomain.IZone;
import com.shephertz.app42.server.message.WarpResponseResultCode;

import java.util.Collection;


/**
 *
 * @author shephertz
 */
public class ExampleZoneAdaptor extends BaseZoneAdaptor {
    private IZone izone;

    ExampleZoneAdaptor(IZone izone){
        this.izone = izone;
    }

    public ExampleZoneAdaptor() {
        super();
    }

    @Override
    public void handleCreateRoomRequest(IUser user, IRoom room, HandlingResult result) {
        System.out.println("Create room request");
        super.handleCreateRoomRequest(user, room, result);

    }

    @Override
    public void handleDeleteRoomRequest(IUser user, IRoom room, HandlingResult result) {
        super.handleDeleteRoomRequest(user, room, result);
    }

    @Override
    public void handleAddUserRequest(IUser user, String authData, HandlingResult result) {
        System.out.println("handleAddUserRequest");
        super.handleAddUserRequest(user, authData, result);
    }

    @Override
    public void handleResumeUserRequest(IUser user, String authData, HandlingResult result) {
        super.handleResumeUserRequest(user, authData, result);
    }

    @Override
    public void onTimerTick(long time) {
        System.out.println("Timertick zoneadaptor");
        Collection<IUser> users = izone.getUsers();
        for(IUser user: users) {
            System.out.println(user.getName());
            System.out.println(user.getLocation().getName());
            System.out.println(user.getLocation().getId());
        }
        super.onTimerTick(time);
    }

    @Override
    public void onUserRemoved(IUser user) {
        System.out.println("onUserRemoved");
        super.onUserRemoved(user);
    }

    @Override
    public void onUserPaused(IUser user) {
        super.onUserPaused(user);
    }

    @Override
    public void handlePrivateChatRequest(IUser sender, IUser toUser, HandlingResult result) {
        super.handlePrivateChatRequest(sender, toUser, result);
    }

    @Override
    public void onAdminRoomAdded(IRoom room) {
        System.out.println("Create onAdminRoomAdded request");
        super.onAdminRoomAdded(room);
    }

    @Override
    public void onAdminRoomDeleted(IRoom room) {
        super.onAdminRoomDeleted(room);
    }
}
