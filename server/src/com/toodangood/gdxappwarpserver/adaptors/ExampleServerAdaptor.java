package com.toodangood.gdxappwarpserver.adaptors;

import com.shephertz.app42.server.idomain.BaseServerAdaptor;
import com.shephertz.app42.server.idomain.IRoom;
import com.shephertz.app42.server.idomain.IZone;

import java.util.HashMap;


public class ExampleServerAdaptor extends BaseServerAdaptor{

    @Override
    public void onZoneCreated(IZone zone)
    {
        System.out.println("onZoneCreated:" + zone.getName());
        zone.setAdaptor(new ExampleZoneAdaptor(zone));
        IRoom room = zone.createRoom("demo-room", 5, new HashMap<>());
        room.setAdaptor(new ExampleRoomAdaptor(zone, room));
    }



}
