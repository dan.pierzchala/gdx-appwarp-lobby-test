package com.toodangood.gdxappwarpserver.adaptors;

import com.shephertz.app42.server.idomain.BaseRoomAdaptor;
import com.shephertz.app42.server.idomain.HandlingResult;
import com.shephertz.app42.server.idomain.IRoom;
import com.shephertz.app42.server.idomain.IUser;
import com.shephertz.app42.server.idomain.IZone;


public class ExampleRoomAdaptor extends BaseRoomAdaptor {
    private IZone izone;
    private IRoom gameRoom;

    public ExampleRoomAdaptor(IZone izone, IRoom room) {
        this.izone = izone;
        this.gameRoom = room;
    }

    @Override
    public void onUserLeaveRequest(IUser user) {
        System.out.println("onUserLeaveRequest");
        super.onUserLeaveRequest(user);
    }

    @Override
    public void handleUserJoinRequest(IUser user, HandlingResult result) {
        System.out.println("handleUserJoinRequest");
        super.handleUserJoinRequest(user, result);
    }
}
