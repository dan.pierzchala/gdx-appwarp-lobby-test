package com.toodangood.gdxappwarpserver;

import com.shephertz.app42.server.AppWarpServer;
import com.toodangood.gdxappwarpserver.adaptors.ExampleServerAdaptor;

public class Main {

    public static void main(String[] args) throws Exception {
	String appconfigPath = System.getProperty("user.dir")+System.getProperty("file.separator")+"AppConfig.json";
	boolean started = AppWarpServer.start(new ExampleServerAdaptor(), appconfigPath);
        if(!started){
            throw new Exception("AppWarpServer did not start. See logs for details.");
        }
    }
}
