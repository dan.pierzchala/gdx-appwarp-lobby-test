# gdx-appwarp-lobby-test

This repo contains both a libgdx based appwarps2 client and an appwarps2 server

This is a minimal sample to show buggy behavior in the AppwarpS2 java client sdk, mainly regarding using the [`Lobby`](http://appwarps2.shephertz.com/dev-center/basic-concepts/#lobby)

**The bug relates to what looks like incorrect error codes being returned by the server when interacting with the lobby from the client**



*For testing purposes only, the server will repeatedly log all connected `users` and their `locations (room or lobby id)` on each `timerTick`
The server will also automatically create a single `demo-room` for testing purposes on startup.*

*The client contains buttons to perform various actions, and labels to display data such as room ids and result codes*

*A zone has already been created via the admin dashboard, and the client is using that zone id to connect to the server instance. The server is running on 127.0.0.1:12346*

To run the client:
- From inside the `client/` directory, run the desktop libgdx app with `./gradlew lwjgl3:run`

The client app looks like:

![client app picture](https://i.ibb.co/BgTzb4B/Selection-103.png)

To run the server:
- From inside the `server/` directory, run the server as normal, with `ant -f warp.xml`


The test client features include:
- Connecting/Disconnecting from the main server
- Joining/Leaving a room, as well as Get All Rooms for easier testing
- Joining/Leaving the lobby, as well as Get Lobby Info to determine how many users are currently in the lobby
- There is also a final feature which will get the current location of the client user, as returned by the server

The test client **does not** include anything relating to subscribing, sending messages, or anything else.

To reproduce the `ERROR 6` code, do the following.
1) Start the server with `ant -f warp.xml` from inside the `server/` directory
2) Start the client with `./gradlew lwjgl3:run` from inside the `client/` directory
3) Click the `Connect` button on the client. You should see the connection status label update to reflect that you are now connected.  You will also begin to see output from the server that looks like: `[java] testuser`, since the client is now connected, but not in a location

*now that we are connected to the server, lets try to join the room that was automatically created by the server, to make sure the client can join rooms as expected*

4) Click the `Get Rooms` button. Notice that the room id is populated on the client. (if the room didn't appear, it was probably considered a ghost room and deleted by the server, make sure you don't wait too long after starting the server to join the room)
5) Now, click the `join room: <room id>` button.  Notice that the room response code is `0 (success)`.  You will also notice that the output from the server now includes a both a `location name` and `id` for `testuser`
6) Click the 'Get my location' button.  This will call a `getLiveUserInfo(clientUsername)` and return the location id from the server.  This will be the same room id that you just joined.

*now that we have confirmed joining the rooms works as expected let's try using the lobby features*

7) Click the 'Get my location' button once again to confirm that you are located inside of a room (the room id is more than a single digit)
8) Now, click `Get Lobby Info`, this will call `getLiveLobbyInfo` and will update the `Lobby User Count:` label with the amount of users in the lobby.  You will see `0`, as expected.
9) Click `Join Lobby`.  Notice on the server that the user's location output has correctly changed to `testuser mainLobby 0`, from `tester demo-room <somelongroomid>`. You will also notice on the client that the `Lobby response code` label shows `6`, which is defined as `UNKNOWN_ERR` 
10) However, the client seems to have successfully joined the lobby, even though we have an error as a response code.  We can confirm this with the `Get My Location` button from before.  Clicking this will update the location label to show `0` (the id of the lobby I guess) as expected.
11) We can also confirm that the client has joined the lobby by clicking the `Get Lobby Info` button, which will now show there being `1` user in the lobby, which is the client.

So, it looks like the client is able to join the lobby, but the result code is always bad.

We can also further confirm that the client can only be joined to a single location at any time.
1) Join the lobby.
2) Click `Get Lobby Info` to confirm that there is `1` user in the lobby.
3) Join the room.
4) Click 'Get Lobby Info', to confirm that there are `0` users in the lobby. The user was automatically removed from the lobby when joining a room.